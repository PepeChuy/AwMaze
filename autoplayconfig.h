#ifndef AUTOPLAYCONFIG_H
#define AUTOPLAYCONFIG_H

#include <QDialog>
#include <QButtonGroup>
#include <QRadioButton>

#include "mazectrl.h"

namespace Ui {
class AutoPlayConfig;
}

class AutoPlayConfig : public QDialog
{
    Q_OBJECT

    public:
        explicit AutoPlayConfig(QWidget *parent = nullptr);
        ~AutoPlayConfig();

    signals:
        void setNeighborsOrder(std::vector<int>);
        void setDistanceType(int);
        void setRepeatNodes(bool);
        void playUniformCost();
        void playGreedy();
        void playAStar();

    public slots:
        void execMe();

    private slots:
        void on_startButton_clicked();

    private:
        Ui::AutoPlayConfig *ui;
        QButtonGroup* gameModesBg;
        QButtonGroup* distanceBg;

};

#endif // AUTOPLAYCONFIG_H
