#include "loadmazedialog.h"
#include "ui_loadmazedialog.h"

#include <QStandardPaths>
#include <QFileDialog>
#include <QColorDialog>
#include <QRandomGenerator>
#include <QHash>

const QString LoadMazeDialog::home = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);

LoadMazeDialog::LoadMazeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadMazeDialog)
{
    ui->setupUi(this);
    ui->label_err->setVisible(false);
    QObject::connect(ui->tableWidget_lands, &QTableWidget::cellClicked, [this](int r, int c)
    {
        if (c != 2) return;
        auto color = QColorDialog::getColor(Qt::white, this);
        if (color.isValid()) ui->tableWidget_lands->item(r, c)->setBackgroundColor(color);
    });
}

LoadMazeDialog::~LoadMazeDialog()
{
    delete ui;
}

void LoadMazeDialog::showErr(QString errDesc)
{
    ui->lineEdit_fName->clear();
    clearLands();
    ui->label_err->setText(errDesc);
    ui->label_err->setVisible(true);
    ui->pushButton_ok->setEnabled(false);
}

void LoadMazeDialog::askLandsConfig(QString fileN, const std::set < int > &lands)
{
    ui->lineEdit_fName->setText(fileN);
    clearLands();
    ui->label_err->setVisible(false);
    auto i = 0;
    for (const auto l : lands)
    {
        auto id = new QTableWidgetItem(QString::number(l));
        auto name = new QLineEdit();
        auto pattern = new QTableWidgetItem();
        id->setFlags(id->flags() & ~Qt::ItemIsEnabled);
        pattern->setFlags(pattern->flags() & ~Qt::ItemIsEnabled);
        pattern->setBackgroundColor(QColor::fromRgb(QRandomGenerator::global()->generate()));
        ui->tableWidget_lands->insertRow(i);
        ui->tableWidget_lands->setItem(i, 0, id);
        ui->tableWidget_lands->setCellWidget(i, 1, name);
        ui->tableWidget_lands->setItem(i++, 2, pattern);
    }
    ui->tableWidget_lands->cellWidget(0, 1)->setFocus();
    ui->pushButton_ok->setEnabled(true);
}

void LoadMazeDialog::execMe()
{
    this->exec();
}

void LoadMazeDialog::on_pushButton_load_clicked()
{
    auto path = QFileDialog::getOpenFileName(this, "Load Maze File", home, "CSV Files (*.csv)");
    if (!path.isEmpty()) emit loadMaze(path);
}

void LoadMazeDialog::on_pushButton_ok_clicked()
{
    auto fail = [this](const QString err){
        ui->label_err->setText(err);
        ui->label_err->setVisible(true);
    };
    auto table = ui->tableWidget_lands;
    QHash < QString, int > names;
    std::vector < Land > lands;
    for (auto i = 0; i < ui->tableWidget_lands->rowCount(); ++i)
    {
        auto currStr = static_cast < QLineEdit * > (table->cellWidget(i, 1))->text();
        if (currStr.isEmpty())
        {
            fail(QString("Empty name at row: %1").arg(i + 1));
            return;
        }
        auto it = names.constFind(currStr);
        if (it != names.constEnd())
        {
            fail(QString("Duplicated land names at rows: %1 and %2!").arg(it.key()).arg(i + 1));
            return;
        }
        names[currStr] = i + 1;
        auto id = table->item(i, 0)->text().toInt();
        auto pattern = table->item(i, 2)->backgroundColor().name();
        lands.push_back(Land(id, currStr, Land::Color, pattern));
    }
    ui->label_err->setVisible(false);
    emit setupLandsCompleted(lands);
    accept();
}

void LoadMazeDialog::clearLands()
{
    ui->tableWidget_lands->clearContents();
    ui->tableWidget_lands->setRowCount(0);
}
