#include "mazenodeinfo.h"
#include "ui_mazenodeinfo.h"

MazeNodeInfo::MazeNodeInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MazeNodeInfo)
{
    ui->setupUi(this);
}

MazeNodeInfo::~MazeNodeInfo()
{
    delete ui;
}

void MazeNodeInfo::execMe(const MazeNode& mazenode, int x, int y, bool isInit, bool isFinal)
{
    std::vector < QString > colNames =
    {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ"};
    ui->label_key->setText(colNames[static_cast<unsigned long long>(x)] + QString::number(y+1));
    ui->label_type->setText(mazenode.land()->name());
    QString visitsString = "";
    for (auto visit : mazenode.visits())
    {
        visitsString += QString::number(visit) + ", ";
    }
    visitsString.chop(2);
    ui->label_visits->setText(visitsString);
    if (isInit)
    {
        ui->label_initial->setText("True");
    }
    else
    {
        ui->label_initial->setText("False");
    }
    if (isFinal)
    {
        ui->label_final->setText("True");
    }
    else
    {
        ui->label_final->setText("False");
    }
    this->exec();
}
