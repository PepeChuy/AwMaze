#include "being.h"

Being::Being()
{
    this->name = "noname";
    this->costs = std::map<int, double>();
    this->iconPath = ":/resources/icon.png";
}

Being::Being(const QString &name, const std::map<int, double> &costs, const QString &iconPath)
{
    this->name = name;
    this->costs = costs;
    this->iconPath = iconPath;
}

const QString &Being::getName() const
{
    return name;
}

void Being::setName(const QString &value)
{
    name = value;
}

const std::map<int, double> &Being::getCosts() const
{
    return costs;
}

void Being::setCosts(const std::map<int, double> &value)
{
    costs = value;
}

const QString& Being::getIconPath() const
{
    return iconPath;
}

void Being::setIconPath(const QString& iconPath)
{
    this->iconPath = iconPath;
}
