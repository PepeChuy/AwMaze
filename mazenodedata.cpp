#include "mazenodedata.h"

using MazeNodeType = MazeNodeData::MazeNodeType;
using LandItr = MazeNodeData::LandItr;

MazeNodeData::MazeNodeData(MazeNodeType type, int x, int y, int nVisit, LandItr land,
                           double unitCost, double gn, double hn)
    : m_type(type), m_foundPathMember(false), m_x(x), m_y(y), m_nVisit(nVisit), m_land(land),
      m_unitCost(unitCost), m_gn(gn), m_hn(hn) {}

MazeNodeType MazeNodeData::type() const { return m_type; }

bool MazeNodeData::foundPathMember() const { return m_foundPathMember; }

int MazeNodeData::x() const { return m_x; }

int MazeNodeData::y() const { return m_y; }

int MazeNodeData::nVisit() const { return m_nVisit; }

LandItr MazeNodeData::land() const { return m_land; }

double MazeNodeData::cost() const { return m_unitCost; }

double MazeNodeData::fn() const { return m_gn + m_hn; }

double MazeNodeData::gn() const { return m_gn; }

double MazeNodeData::hn() const { return m_hn; }

void MazeNodeData::setType(MazeNodeType type) { m_type = type; }

void MazeNodeData::setFoundPathMember(bool member) { m_foundPathMember = member; }

void MazeNodeData::setX(int x) { m_x = x; }

void MazeNodeData::setY(int y) { m_y = y; }

void MazeNodeData::setNVisit(int nVisit) { m_nVisit = nVisit; }

void MazeNodeData::setLand(LandItr land) { m_land = land; }

void MazeNodeData::setCost(int unitCost) { m_unitCost = unitCost; }

void MazeNodeData::setGN(double gn) { m_gn = gn; }

void MazeNodeData::setHN(double hn) { m_hn = hn; }
