#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <mazescene.h>

#include <QButtonGroup>
#include <QRadioButton>
#include <QLabel>
#include <QProcess>
#include <QtGui>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->canvas->setScene(new MazeScene());
    ui->widget_beings->setVisible(false);
    init = final = being = nullptr;

    connect(static_cast < MazeScene * > (ui->canvas->scene()), &MazeScene::itemClicked,
            this, &MainWindow::processItemClick);

    up = new QShortcut(Qt::Key_Up, this);
    right = new QShortcut(Qt::Key_Right, this);
    down = new QShortcut(Qt::Key_Down, this);
    left = new QShortcut(Qt::Key_Left, this);
    nextSong = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Right), this);
    prevSong = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Left), this);
    volumeUp = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Up), this);
    volumeDown = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Down), this);
    connect(up, &QShortcut::activated, this, [this](){emit moveUp();});
    connect(right, &QShortcut::activated, this, [this](){emit moveRight();});
    connect(down, &QShortcut::activated, this, [this](){emit moveDown();});
    connect(left, &QShortcut::activated, this, [this](){emit moveLeft();});


    connect(nextSong, &QShortcut::activated, this, [this](){mediaPlaylist->next();});
    connect(prevSong, &QShortcut::activated, this, [this](){mediaPlaylist->previous();});
    connect(volumeUp, &QShortcut::activated, this, [this](){
        mediaPlayer->setVolume(currentVolume < 100 ? currentVolume += 5 : 100);
    });
    connect(volumeDown, &QShortcut::activated, this, [this](){
        mediaPlayer->setVolume(currentVolume > 0 ? currentVolume -= 5 : 0);
    });

    // Draws cover page maze
    Maze m;
    m.setLands({
        Land(0, "background", Land::Color, "#000000"),
        Land(1, "c1", Land::Color, "#ff7f50"),
        Land(2, "c2", Land::Color, "#97ffff"),
        Land(3, "c3", Land::Color, "#9bcd9b"),
        Land(4, "c4", Land::Color, "#ff4040"),
        Land(5, "c5", Land::Color, "#1e90ff"),
        Land(6, "c6", Land::Color, "#7fff00"),
        Land(7, "c7", Land::Color, "#18a303"),
        Land(8, "c8", Land::Color, "#eee8cd"),
        Land(9, "c9", Land::Color, "#008b8b"),
        Land(10, "c10", Land::Color, "#92e285"),
        Land(11, "c11", Land::Color, "#cdb79e"),
        Land(12, "c12", Land::Color, "#43c330"),
        Land(13, "c13", Land::Color, "#ee6a50"),
        Land(14, "c14", Land::Color, "#6e8b3d"),
        Land(15, "c15", Land::Color, "#f09e6f"),
        Land(16, "c16", Land::Color, "#7fffd4"),
    });
    m.initGrid({
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 1, 1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0},
        {0, 0, 4, 0, 0, 1, 0, 0, 2, 0, 5, 0, 3, 0, 0},
        {0, 0, 4, 6, 6, 6, 0, 0, 2, 0, 5, 0, 3, 0, 0},
        {0, 0, 4, 0, 0, 7, 0, 0, 8, 0, 5, 0, 3, 0, 0},
        {0, 0, 4, 0, 0, 7, 0, 0, 0, 8, 0, 9, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 7, 0, 0, 0, 7, 0, 0, 7, 7, 10, 0, 0, 11, 0},
        {0, 12, 7, 0, 7, 7, 0, 0, 0, 0, 7, 0, 0, 11, 0},
        {0, 12, 0, 13, 0, 13, 0, 0, 0, 7, 0, 0, 0, 11, 0},
        {0, 14, 0, 0, 0, 13, 0, 0, 15, 0, 0, 0, 0, 0, 0},
        {0, 14, 0, 0, 0, 7, 0, 0, 15, 15, 7, 0, 0, 16, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    });
    drawMaze(m);

    currentVolume = 30;
    mediaPlaylist = new QMediaPlaylist();
    mediaPlaylist->addMedia(QUrl("qrc:/resources/music/allstar8bit.mp3"));
    mediaPlaylist->addMedia(QUrl("qrc:/resources/music/nevergonna8bit.mp3"));
    mediaPlaylist->addMedia(QUrl("qrc:/resources/music/takeonme8bit.mp3"));
    mediaPlaylist->addMedia(QUrl("qrc:/resources/music/shootingstart8bit.mp3"));
    mediaPlaylist->addMedia(QUrl("qrc:/resources/music/curbyourenthusiasm8bit.mp3"));
    mediaPlaylist->setPlaybackMode(QMediaPlaylist::Loop);
    mediaPlayer = new QMediaPlayer();
    mediaPlayer->setPlaylist(mediaPlaylist);
    mediaPlayer->setVolume(currentVolume);
    mediaPlayer->play();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionLoadMaze_triggered()
{
    emit showLoadDlg();
}

void MainWindow::on_actionClearGame_triggered()
{
    clearGame();
}

void MainWindow::on_addBeingButton_clicked()
{
    emit showBeingDlg();
}

void MainWindow::setBeingsRadios(std::vector<Being>& beings)
{
    bg = new QButtonGroup(this);
    bg->setExclusive(true);
    auto beingsToDelete = ui->groupBox_beings->layout()->count();
    for (auto i = 0; i < beingsToDelete; ++i)
    {
        delete ui->groupBox_beings->layout()->itemAt(0)->widget();
    }
    for (unsigned long long i = 0; i < beings.size(); i++)
    {
        auto currentBeing = beings[i];
        QRadioButton *radioButton = new QRadioButton(tr(currentBeing.getName().toStdString().c_str()));
        radioButton->setIconSize(QSize(32, 32));
        radioButton->setIcon(QIcon(QPixmap(currentBeing.getIconPath())));
        connect(radioButton, &QRadioButton::toggled, this, &MainWindow::onBeingToggled);
        bg->addButton(radioButton);
        bg->setId(radioButton, static_cast<int>(i));
        if (i == beings.size()-1)
        {
            radioButton->setChecked(true);
        }
        ui->groupBox_beings->layout()->addWidget(radioButton);
    }
}

void MainWindow::onBeingToggled(bool toggled)
{
    if (toggled)
    {
        emit selectedBeing(bg->checkedId());
    }
}

void MainWindow::drawMaze(Maze &mz)
{
    auto clearLayout = [](QLayout *l) { while (auto item = l->takeAt(0)) delete item->widget(); };
    auto newQLabel = [](const QString &txt)
    {
        auto l = new QLabel();
        l->setText(txt);
        l->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
        return l;
    };
    std::vector < QString > colNames =
    {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ"};
    std::vector < int > colSizes =
    {-1, 300, 200, 150, 120, 100, 85, 75, 66, 60, 54, 50, 46, 42, 40};

    QPen pen;
    bool isCoverPage;
    std::function< void(QLabel *) > addRowH = [this] (QLabel *l) { ui->header_rows->addWidget(l); };
    if (cells.empty())
    {
        isCoverPage = true;
        pen = Qt::NoPen;
        addRowH = [] (QLabel *) {};
    }
    else
    {
        isCoverPage = false;
        pen = Qt::SolidLine;
    }

    cells.clear();
    ui->canvas->scene()->clear();
    init = final = being = nullptr;
    togglePlayAndShowTreeActions(false);
    toggleSelectStates(false);
    clearLayout(ui->header_cols);
    clearLayout(ui->header_rows);
    colsN = mz.nodes()[0].size();
    rowsN = mz.nodes().size();
    nodeSz = colSizes[std::max(rowsN, colsN) - 1];
    masks = std::vector < std::vector < QGraphicsRectItem * > >
            (colsN, std::vector < QGraphicsRectItem * > (rowsN));
    visitsTexts = std::vector < std::vector < std::vector < QGraphicsTextItem * > > >
                  (colsN, std::vector < std::vector < QGraphicsTextItem *  > > (rowsN));
    // Resizes canvas and scene to fit contents
    auto sceneW = nodeSz * static_cast < int > (colsN);
    auto sceneH = nodeSz * static_cast < int > (rowsN);
    ui->canvas->setFixedSize(sceneW, sceneH);
    ui->canvas->setSceneRect(0, 0, sceneW, sceneH);
    adjustSize();

    for (size_t r = 0; r < rowsN; ++r)
    {
        addRowH(newQLabel(QString::number(r + 1)));
        for (size_t c = 0; c < colsN; ++c)
        {
            auto left = static_cast < int > (c) * nodeSz, top = static_cast < int > (r) * nodeSz;
            auto cell = static_cast < QGraphicsItem * > (ui->canvas->scene()->addRect(
                QRect(left, top, nodeSz, nodeSz), pen,
                QBrush(QColor(mz.nodes()[r][c].land()->pattern())))
            );
            cells[cell] = {c, r};
        }
    }

    if (!isCoverPage)
    {
        for (size_t i = 0; i < mz.nodes()[0].size(); ++i)
        {
            ui->header_cols->addWidget(newQLabel(colNames[i]));
        }
    }
}

void MainWindow::showBeingWidget()
{
    ui->widget_beings->setVisible(true);
}

void MainWindow::getSetInitRes(bool succ, int x, int y)
{
    if (succ)
    {
        auto left = x * nodeSz + (static_cast < double > (nodeSz) / 3 * 2),
             top = static_cast < double > (y) * nodeSz;
        if (init == nullptr)
        {
            auto img = QPixmap(":/icon/initial").scaledToHeight(nodeSz / 3,
                                                                Qt::SmoothTransformation);
            init = ui->canvas->scene()->addPixmap(img);
        }
        init->setPos(left, top);
        if (final != nullptr)
        {
            ui->actionPlayMaze->setEnabled(true);
            ui->actionAutoPlay->setEnabled(true);
        }
        ui->statusBar->showMessage("");
    }
    else
    {
        ui->statusBar->showMessage("Can't start from there!");
    }
}

void MainWindow::getSetFinalRes(bool succ, int x, int y)
{
    if (succ)
    {
        auto left = x * nodeSz + (static_cast < double > (nodeSz) / 3 * 2),
             top = static_cast < double > (y) * nodeSz;
        if (final == nullptr)
        {
            auto img = QPixmap(":/icon/final").scaledToHeight(nodeSz / 3, Qt::SmoothTransformation);
            final = ui->canvas->scene()->addPixmap(img);
        }
        final->setPos(left, top);
        if (init != nullptr)
        {
            ui->actionPlayMaze->setEnabled(true);
            ui->actionAutoPlay->setEnabled(true);
        }
        ui->statusBar->showMessage("");
    }
    else
    {
        ui->statusBar->showMessage("Can't finish there!");
    }
}

void MainWindow::maskAllNodes(std::vector < std::pair < int, int > > exceptCoords)
{
    for (size_t c = 0; c < colsN; ++c)
    {
        for (size_t r = 0; r < rowsN; ++r)
        {
            if (masks[c][r] != nullptr) continue;
            auto coord = std::make_pair(static_cast < int > (c), static_cast < int > (r));
            auto foundIt = std::find(exceptCoords.begin(), exceptCoords.end(), coord);
            if (foundIt == exceptCoords.end())
            {
                masks[c][r] = ui->canvas->scene()->addRect(
                    QRect(static_cast < int > (c) * nodeSz, static_cast < int > (r) * nodeSz,
                          nodeSz, nodeSz),
                    Qt::NoPen, QBrush(MASK_COLOR));
            }
        }
    }
}

void MainWindow::unmaskNodes(std::vector < std::pair < int, int > > coords)
{
    for (const auto &c : coords)
    {
        auto x = static_cast < size_t > (c.first), y = static_cast < size_t > (c.second);
        if (masks[x][y] != nullptr)
        {
            ui->canvas->scene()->removeItem(masks[x][y]);
            masks[x][y] = nullptr;
        }
    }
}

void MainWindow::gotSearchTree(QString dotLangRepr)
{
    QProcess dot;
    dot.start("dot", {"-Tsvg", "-o", "out.svg"});
    dot.waitForStarted(-1);
    dot.write(dotLangRepr.toUtf8().constData());
    dot.closeWriteChannel();
    dot.waitForFinished(-1);
    if (dot.exitCode() == 0)
    {
        ui->statusBar->clearMessage();
        emit renderSearchTree("out.svg");
    }
    else
    {
        ui->statusBar->showMessage(QString("Failed to render search tree! ") +
                                   "Make sure you have dot (Graphviz) installed and configured...");
    }
}

void MainWindow::clearGame()
{
    if (init != nullptr) ui->canvas->scene()->removeItem(init);
    if (final != nullptr) ui->canvas->scene()->removeItem(final);
    if (being != nullptr) ui->canvas->scene()->removeItem(being);
    init = final = being = nullptr;
    for (auto &c : masks)
    {
        for (auto &m : c)
        {
            if (m != nullptr) {
                ui->canvas->scene()->removeItem(m);
                m = nullptr;
            }
        }
    }
    for (auto &c : visitsTexts)
    {
        for (auto &r : c)
        {
            for (auto txt : r)
            {
                ui->canvas->scene()->removeItem(txt);
            }
            r.clear();
        }
    }
    ui->actionLoadMaze->setEnabled(true);
    togglePlayAndShowTreeActions(false);
    ui->actionClearGame->setEnabled(false);
    toggleSelectStates(true);
    showBeingWidget();
    emit gameCleared();
}

void MainWindow::moveBeing(int x, int y, std::vector < int > visits)
{
    auto left = x * nodeSz + (static_cast < double > (nodeSz) / 3),
         top = y * nodeSz + (static_cast < double > (nodeSz) / 3);
    being->setPos(left, top);
    renderNodeVisits(x, y, visits);
}

void MainWindow::togglePlayAndShowTreeActions(bool enable)
{
    ui->actionPlayMaze->setEnabled(enable);
    ui->actionAutoPlay->setEnabled(enable);
    ui->actionShowSearchTree->setEnabled(enable);
}

void MainWindow::toggleSelectStates(bool enable)
{
    ui->actionSelectInitial->setEnabled(enable);
    ui->actionSelectFinal->setEnabled(enable);
    ui->actionSelectInitial->setChecked(false);
    ui->actionSelectFinal->setChecked(false);
}

void MainWindow::on_actionSelectInitial_toggled(bool arg1)
{
    if (arg1)
    {
        ui->actionSelectFinal->setChecked(false);
    }
}

void MainWindow::on_actionSelectFinal_toggled(bool arg1)
{
    if (arg1)
    {
        ui->actionSelectInitial->setChecked(false);
    }
}

void MainWindow::processItemClick(QGraphicsItem *item)
{
    int x, y;
    if (ui->actionSelectInitial->isEnabled() and ui->actionSelectInitial->isChecked())
    {
        auto foundIt = cells.find(item);
        if (foundIt != cells.end())
        {
            std::tie(x, y) = foundIt->second;
            emit trySelectInitial(x, y);
        }
    }
    else if (ui->actionSelectFinal->isEnabled() and ui->actionSelectFinal->isChecked())
    {
        auto foundIt = cells.find(item);
        if (foundIt != cells.end())
        {
            std::tie(x, y) = foundIt->second;
            emit trySelectFinal(x, y);
        }
    }
    else
    {
        auto foundIt = cells.find(item);
        if (foundIt != cells.end())
        {
            std::tie(x, y) = foundIt->second;
            emit showInfo(x, y);
        }
    }
}

void MainWindow::on_actionPlayMaze_triggered()
{
    initGame();
    ui->actionClearGame->setEnabled(true);
    emit startedPlaying();
}

void MainWindow::on_actionAutoPlay_triggered()
{
    initGame();
    ui->actionLoadMaze->setEnabled(false);
    emit showAutoPlayDlg();
}

void MainWindow::on_actionShowSearchTree_triggered()
{
    emit retrieveSearchTree();
}

void MainWindow::updateBeingIconPath(const QString &newPath)
{
    beingIconPath = newPath;
}

void MainWindow::processEvents()
{
    qApp->processEvents();
}

void MainWindow::initGame()
{
    toggleSelectStates(false);
    ui->actionPlayMaze->setEnabled(false);
    ui->actionAutoPlay->setEnabled(false);
    ui->widget_beings->setVisible(false);
    ui->actionShowSearchTree->setEnabled(true);
    auto img = QPixmap(beingIconPath).scaledToHeight(nodeSz / 3, Qt::SmoothTransformation);
    being = ui->canvas->scene()->addPixmap(img);
    being->setPos(init->pos().x() - static_cast < double > (nodeSz) / 3,
                  init->pos().y() + static_cast < double > (nodeSz) / 3);
}

void MainWindow::renderNodeVisits(int x, int y, const std::vector < int > &visits)
{
    auto left = x * nodeSz + 1, top = y * nodeSz;
    auto third = static_cast < double > (nodeSz) / 3;
    auto bottom = top + 2 * third - 1;
    auto xIdx = static_cast < size_t > (x), yIdx = static_cast < size_t > (y);
    for (auto txt : visitsTexts[xIdx][yIdx])
    {
        ui->canvas->scene()->removeItem(txt);
    }
    visitsTexts[xIdx][yIdx].clear();
    auto font = QFont();
    font.setPixelSize(std::min(nodeSz / 3 - 1, 20));
    QGraphicsTextItem *text;
    if (visits.size() >= 4)
    {
        text = ui->canvas->scene()->addText("...", font);
        text->document()->setDocumentMargin(0);
        text->setPos(left, top + 1);
        visitsTexts[xIdx][yIdx].push_back(text);
    }
    if (visits.size() >= 3)
    {
        text = ui->canvas->scene()->addText(QString::number(visits[visits.size() - 3]), font);
        text->document()->setDocumentMargin(0);
        text->setPos(left, top + third);
        visitsTexts[xIdx][yIdx].push_back(text);
    }
    if (visits.size() >= 2)
    {
        text = ui->canvas->scene()->addText(QString::number(visits[visits.size() - 2]), font);
        text->document()->setDocumentMargin(0);
        text->setPos(left, bottom);
        visitsTexts[xIdx][yIdx].push_back(text);
    }
    text = ui->canvas->scene()->addText(QString::number(visits[visits.size() - 1]), font);
    text->document()->setDocumentMargin(0);
    text->setPos(left + third, bottom);
    visitsTexts[xIdx][yIdx].push_back(text);
}

void MainWindow::setPlayModesEnabled()
{
    ui->canvas->scene()->removeItem(being);
    ui->actionLoadMaze->setEnabled(true);
    showBeingWidget();
    ui->actionPlayMaze->setEnabled(true);
    ui->actionAutoPlay->setEnabled(true);
    toggleSelectStates(true);
    ui->actionShowSearchTree->setEnabled(false);
}

void MainWindow::setClearGameEnabled()
{
    ui->actionClearGame->setEnabled(true);
    ui->actionLoadMaze->setEnabled(true);
    showBeingWidget();
}
