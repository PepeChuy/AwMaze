#ifndef MAZECTRL_H
#define MAZECTRL_H

#include "land.h"
#include "maze.h"
#include "being.h"
#include "searchtree.h"
#include "searchtreenode.h"
#include "mazenodedata.h"

#include <set>
#include <vector>

#include <QObject>

class MazeCtrl : public QObject
{
    Q_OBJECT

    public:
        MazeCtrl();
        int vis = 0;
        static const int LEFT = 1;
        static const int UP = 2;
        static const int RIGHT = 3;
        static const int DOWN = 4;
        static const int EUCLIDEAN = 1;
        static const int MANHATTAN = 2;
        enum GameMode
        {
            Normal,
            AStar,
            Greedy,
            UniformCost
        };

    signals:
        void succAtLoad(QString fName, const std::set < int > &lands);
        void errAtLoad(QString errDesc);
        void landsInfo(std::vector<Land>);
        void mazeIsReady(Maze &mz);
        void beingIsReady();
        void setInitRes(bool, int, int);
        void setFinalRes(bool, int, int);
        void successfulMovement(int, int, std::vector < int > visits);
        void sentNodeInfo(MazeNode&, int, int, bool, bool);
        void maskAllNodes(std::vector < std::pair < int, int > > exceptCords);
        void unmaskNodes(std::vector < std::pair < int, int > > coords);
        void searchTreeConverted(QString dotLangRepr);
        void processEvents();
        void finishedAutoPlay();

    public slots:
        void loadMazeFromFile(QString path);
        void landMap2Maze(std::vector < Land > lands);
        void sendLandsInfo();
        void setupBeing(const QString &name, const std::map < int, double > &costs);
        void setInit(const int&, const int&);
        void setFinal(const int&, const int&);
        void start();
        void searchTree2DotLang();
        void maskSetup();
        void moveUp();
        void moveRight();
        void moveDown();
        void moveLeft();
        void moveTo(MazeNode*);
        void sendNodeInfo(int, int);
        void solveByUniformCost();
        void solveByGreedy();
        void solveByAStar();
        void setNeighborsOrder(std::vector<int>);
        void setNodeRepeating(bool);
        void setHeuristicType(int);
        void clearGame();

    private:
        static const int MAX_ROWS = 15;
        static const int MAX_COLS = 15;
        static const int MOV_WAIT = 400;
        Maze p_maze;
        Being p_being;
        std::vector < std::vector < int > > p_landMap;
        bool playing;
        GameMode actualGameMode;

        std::vector<int> neighborsOrder = {LEFT, UP, RIGHT, DOWN};
        bool repeatingNodes = false;
        int heuristicType = MANHATTAN;
        SearchTree<MazeNodeData> searchTree;
        SearchTreeNode<MazeNodeData>* currentNode;
        std::vector<SearchTreeNode<MazeNodeData>*> possiblePaths;

        std::vector<MazeNode*> getNeighbors(MazeNode*);
        std::vector<SearchTreeNode<MazeNodeData>*> getPossiblePaths(MazeNode*);
        std::vector < std::pair < int, int > > neighborsCoords(int x, int y);
        std::pair<int, int> getCoordinates(MazeNode*);
        double getHeuristicDistance(MazeNode*, MazeNode*);
        QString tree2DotLang(const SearchTreeNode<MazeNodeData> *,
                             int &lastID, int parentID);
        void markFoundPath(SearchTreeNode<MazeNodeData>*);
};

#endif // MAZECTRL_H
