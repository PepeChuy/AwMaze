#ifndef LOADMAZEDIALOG_H
#define LOADMAZEDIALOG_H

#include "land.h"

#include <vector>
#include <set>

#include <QDialog>

namespace Ui {
    class LoadMazeDialog;
}

class LoadMazeDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit LoadMazeDialog(QWidget *parent = nullptr);
        ~LoadMazeDialog();

    signals:
        void loadMaze(QString path);
        void setupLandsCompleted(std::vector < Land > lands);

    public slots:
        void showErr(QString errDesc);
        void askLandsConfig(QString fileN, const std::set < int > &lands);
        void execMe();

    private slots:
        void on_pushButton_load_clicked();
        void on_pushButton_ok_clicked();

    private:
        void clearLands();

        Ui::LoadMazeDialog *ui;
        static const QString home;
};

#endif // LOADMAZEDIALOG_H
