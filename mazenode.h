#ifndef MAZENODE_H
#define MAZENODE_H

#include "land.h"

#include <vector>

class MazeNode
{
    public:
        using LandItr = std::vector < Land > :: const_iterator;
        MazeNode();
        explicit MazeNode(LandItr land);

        LandItr land() const;
        const std::vector < int > &visits() const;
        void addVisit(int step);

        void setLand(LandItr land);

        void resetVisits();

    private:
        LandItr p_land;
        std::vector < int > p_visits;
};

#endif // MAZENODE_H
