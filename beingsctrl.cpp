#include "beingsctrl.h"

BeingsCtrl::BeingsCtrl()
{

}

void BeingsCtrl::addBeing(Being& newBeing)
{
    this->beings.push_back(newBeing);
    emit setBeingsRadios(this->beings);
}

void BeingsCtrl::selectedBeing(int beingIndex)
{
    emit setCosts(beings[static_cast<unsigned long long>(beingIndex)].getName(), beings[static_cast<unsigned long long>(beingIndex)].getCosts());
    emit newBeingPath(beings[static_cast<unsigned long long>(beingIndex)].getIconPath());
}

void BeingsCtrl::requestBeings()
{
    emit sendBeings(this->beings);
}

void BeingsCtrl::deleteBeings(const Maze&)
{
    this->beings = std::vector<Being>();
    emit setBeingsRadios(this->beings);
}
