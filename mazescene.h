#ifndef MAZESCENE_H
#define MAZESCENE_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>

class MazeScene : public QGraphicsScene
{
    Q_OBJECT

    public:
        explicit MazeScene(QObject *parent = nullptr);

    signals:
        void itemClicked(QGraphicsItem *item);

    protected:
        void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
};

#endif // MAZESCENE_H
