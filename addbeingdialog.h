#ifndef ADDBEINGDIALOG_H
#define ADDBEINGDIALOG_H

#include "being.h"
#include "land.h"

#include <QDialog>
#include <QButtonGroup>

namespace Ui {
class AddBeingDialog;
}

class AddBeingDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit AddBeingDialog(QWidget *parent = nullptr);
        ~AddBeingDialog();

    signals:
        void requestLands();
        void requestBeings();
        void addBeing(Being&);

    public slots:
        void execMe();
        void retrieveLands(std::vector<Land>);
        void retrieveBeings(std::vector<Being>&);

    private slots:
        void on_button_addBeing_clicked();

    private:
        static const int MAX_INPUT_LEN = 8;
        void showErr(QString errDesc);
        Ui::AddBeingDialog *ui;
        std::vector<Being> beings;
        std::vector<Land> lands;
        QButtonGroup* bg;
};

#endif // ADDBEINGDIALOG_H
