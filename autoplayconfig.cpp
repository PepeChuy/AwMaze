#include "autoplayconfig.h"
#include "ui_autoplayconfig.h"

AutoPlayConfig::AutoPlayConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AutoPlayConfig)
{
    ui->setupUi(this);

    gameModesBg = new QButtonGroup(this);
    gameModesBg->setExclusive(true);
    QRadioButton *radioButton = new QRadioButton();
    radioButton->setChecked(true);
    radioButton->setText("Uniform Cost");
    gameModesBg->addButton(radioButton);
    gameModesBg->setId(radioButton, 0);
    ui->groupBox_gameMode->layout()->addWidget(radioButton);

    radioButton = new QRadioButton();
    radioButton->setText("Greedy");
    gameModesBg->addButton(radioButton);
    gameModesBg->setId(radioButton, 1);
    ui->groupBox_gameMode->layout()->addWidget(radioButton);

    radioButton = new QRadioButton();
    radioButton->setText("A*");
    gameModesBg->addButton(radioButton);
    gameModesBg->setId(radioButton, 2);
    ui->groupBox_gameMode->layout()->addWidget(radioButton);



    distanceBg = new QButtonGroup(this);
    distanceBg->setExclusive(true);
    radioButton = new QRadioButton();
    radioButton->setChecked(true);
    radioButton->setText("Euclidean");
    distanceBg->addButton(radioButton);
    distanceBg->setId(radioButton, MazeCtrl::EUCLIDEAN);
    ui->groupBox_Distance->layout()->addWidget(radioButton);

    radioButton = new QRadioButton();
    radioButton->setText("Manhattan");
    distanceBg->addButton(radioButton);
    distanceBg->setId(radioButton, MazeCtrl::MANHATTAN);
    ui->groupBox_Distance->layout()->addWidget(radioButton);

    ui->tableNeighborOrder->horizontalHeader()->setSectionsMovable(true);
}

AutoPlayConfig::~AutoPlayConfig()
{
    delete ui;
}

void AutoPlayConfig::execMe()
{
    this->exec();
}

void AutoPlayConfig::on_startButton_clicked()
{
    emit setRepeatNodes(ui->checkbox_repeat->isChecked());
    emit setDistanceType(distanceBg->checkedId());

    std::vector < int > neighOrder;
    for (auto i = 0; i < 4; ++i)
    {
        for (auto j = 0; j < 4; ++j)
        {
            if (ui->tableNeighborOrder->visualColumn(j) == i)
            {
                neighOrder.push_back(j + 1);
            }
        }
    }
    emit setNeighborsOrder(neighOrder);

    accept();
    switch (gameModesBg->checkedId())
    {
        case 0:
            emit playUniformCost();
            break;
        case 1:
            emit playGreedy();
            break;
        case 2:
            emit playAStar();
            break;
    }
}
