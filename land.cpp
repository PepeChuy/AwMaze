#include "land.h"

using PatternType = Land::PatternType;

Land::Land() {}

Land::Land(int id, QString name, PatternType type, QString pattern)
    : p_id(id), p_name(name), p_type(type), p_pattern(pattern) {}

int Land::id() const { return p_id; }

const QString &Land::name() const { return p_name; }

PatternType Land::type() const { return p_type; }

const QString &Land::pattern() const { return p_pattern; }

void Land::setId(int id) { p_id = id; }

void Land::setName(QString name) { p_name = name; }

void Land::setType(PatternType type) { p_type = type; }

void Land::setPattern(QString pattern) { p_pattern = pattern; }
