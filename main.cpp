#include "mainwindow.h"

#include "loadmazedialog.h"
#include "addbeingdialog.h"
#include "svgviewerwindow.h"

#include "beingsctrl.h"
#include "mazectrl.h"
#include "mazenodeinfo.h"
#include "autoplayconfig.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MazeCtrl mazeCtrl;
    BeingsCtrl beingsCtrl;

    MainWindow w;
    LoadMazeDialog loadDlg;
    AddBeingDialog beingDlg;
    MazeNodeInfo mazeNodeInfo;
    SvgViewerWindow treeWindow;
    AutoPlayConfig autoPlayconfig;

    QObject::connect(&w, &MainWindow::showLoadDlg, &loadDlg, &LoadMazeDialog::execMe);
    QObject::connect(&w, &MainWindow::showBeingDlg, &beingDlg, &AddBeingDialog::execMe);
    QObject::connect(&w, &MainWindow::showAutoPlayDlg, &autoPlayconfig, &AutoPlayConfig::execMe);
    QObject::connect(&w, &MainWindow::renderSearchTree, &treeWindow, &SvgViewerWindow::loadFile);

    QObject::connect(&loadDlg, &LoadMazeDialog::loadMaze, &mazeCtrl, &MazeCtrl::loadMazeFromFile);
    QObject::connect(&loadDlg, &LoadMazeDialog::setupLandsCompleted,
                     &mazeCtrl, &MazeCtrl::landMap2Maze);
    QObject::connect(&mazeCtrl, &MazeCtrl::errAtLoad, &loadDlg, &LoadMazeDialog::showErr);
    QObject::connect(&mazeCtrl, &MazeCtrl::succAtLoad, &loadDlg, &LoadMazeDialog::askLandsConfig);

    QObject::connect(&w, &MainWindow::trySelectInitial, &mazeCtrl, &MazeCtrl::setInit);
    QObject::connect(&w, &MainWindow::trySelectFinal, &mazeCtrl, &MazeCtrl::setFinal);
    QObject::connect(&w, &MainWindow::startedPlaying, &mazeCtrl, &MazeCtrl::start);
    QObject::connect(&w, &MainWindow::retrieveSearchTree, &mazeCtrl, &MazeCtrl::searchTree2DotLang);
    QObject::connect(&w, &MainWindow::moveUp, &mazeCtrl, &MazeCtrl::moveUp);
    QObject::connect(&w, &MainWindow::moveRight, &mazeCtrl, &MazeCtrl::moveRight);
    QObject::connect(&w, &MainWindow::moveDown, &mazeCtrl, &MazeCtrl::moveDown);
    QObject::connect(&w, &MainWindow::moveLeft, &mazeCtrl, &MazeCtrl::moveLeft);

    QObject::connect(&mazeCtrl, &MazeCtrl::mazeIsReady, &w, &MainWindow::showBeingWidget);
    QObject::connect(&mazeCtrl, &MazeCtrl::mazeIsReady, &w, &MainWindow::drawMaze);
    QObject::connect(&mazeCtrl, &MazeCtrl::mazeIsReady, &beingsCtrl, &BeingsCtrl::deleteBeings);
    QObject::connect(&mazeCtrl, &MazeCtrl::beingIsReady, &w, &MainWindow::clearGame);
    QObject::connect(&mazeCtrl, &MazeCtrl::setInitRes, &w, &MainWindow::getSetInitRes);
    QObject::connect(&mazeCtrl, &MazeCtrl::setFinalRes, &w, &MainWindow::getSetFinalRes);
    QObject::connect(&mazeCtrl, &MazeCtrl::maskAllNodes, &w, &MainWindow::maskAllNodes);
    QObject::connect(&mazeCtrl, &MazeCtrl::unmaskNodes, &w, &MainWindow::unmaskNodes);
    QObject::connect(&mazeCtrl, &MazeCtrl::successfulMovement, &w, &MainWindow::moveBeing);
    QObject::connect(&mazeCtrl, &MazeCtrl::searchTreeConverted, &w, &MainWindow::gotSearchTree);

    QObject::connect(&beingDlg, &AddBeingDialog::requestLands, &mazeCtrl, &MazeCtrl::sendLandsInfo);
    QObject::connect(&mazeCtrl, &MazeCtrl::landsInfo, &beingDlg, &AddBeingDialog::retrieveLands);

    QObject::connect(&beingsCtrl, &BeingsCtrl::setCosts, &mazeCtrl, &MazeCtrl::setupBeing);

    QObject::connect(&beingsCtrl, &BeingsCtrl::setBeingsRadios, &w, &MainWindow::setBeingsRadios);
    QObject::connect(&w, &MainWindow::selectedBeing, &beingsCtrl, &BeingsCtrl::selectedBeing);
    QObject::connect(&beingsCtrl, &BeingsCtrl::newBeingPath, &w, &MainWindow::updateBeingIconPath);

    QObject::connect(&beingDlg, &AddBeingDialog::requestBeings,
                     &beingsCtrl, &BeingsCtrl::requestBeings);
    QObject::connect(&beingsCtrl, &BeingsCtrl::sendBeings, &beingDlg,
                     &AddBeingDialog::retrieveBeings);

    QObject::connect(&beingDlg, &AddBeingDialog::addBeing, &beingsCtrl, &BeingsCtrl::addBeing);

    QObject::connect(&w, &MainWindow::showInfo, &mazeCtrl, &MazeCtrl::sendNodeInfo);
    QObject::connect(&mazeCtrl, &MazeCtrl::sentNodeInfo, &mazeNodeInfo, &MazeNodeInfo::execMe);

    QObject::connect(&w, &MainWindow::solveByUniformCost, &mazeCtrl, &MazeCtrl::solveByUniformCost);
    QObject::connect(&w, &MainWindow::solveByGreedy, &mazeCtrl, &MazeCtrl::solveByGreedy);
    QObject::connect(&w, &MainWindow::solveByAStar, &mazeCtrl, &MazeCtrl::solveByAStar);

    QObject::connect(&mazeCtrl, &MazeCtrl::processEvents, &w, &MainWindow::processEvents);

    QObject::connect(&autoPlayconfig, &AutoPlayConfig::rejected,
                     &w, &MainWindow::setPlayModesEnabled);
    QObject::connect(&autoPlayconfig, &AutoPlayConfig::setRepeatNodes, &mazeCtrl, &MazeCtrl::setNodeRepeating);
    QObject::connect(&autoPlayconfig, &AutoPlayConfig::setNeighborsOrder, &mazeCtrl, &MazeCtrl::setNeighborsOrder);
    QObject::connect(&autoPlayconfig, &AutoPlayConfig::setDistanceType, &mazeCtrl, &MazeCtrl::setHeuristicType);
    QObject::connect(&autoPlayconfig, &AutoPlayConfig::playAStar, &mazeCtrl, &MazeCtrl::solveByAStar);
    QObject::connect(&autoPlayconfig, &AutoPlayConfig::playGreedy, &mazeCtrl, &MazeCtrl::solveByGreedy);
    QObject::connect(&autoPlayconfig, &AutoPlayConfig::playUniformCost, &mazeCtrl, &MazeCtrl::solveByUniformCost);

    QObject::connect(&mazeCtrl, &MazeCtrl::finishedAutoPlay, &w, &MainWindow::setClearGameEnabled);

    QObject::connect(&w, &MainWindow::gameCleared, &mazeCtrl, &MazeCtrl::clearGame);

    w.show();

    return a.exec();
}
