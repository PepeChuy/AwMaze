#include "mazenode.h"

using LandItr = MazeNode::LandItr;

MazeNode::MazeNode() {}

MazeNode::MazeNode(LandItr land) : p_land(land) {}

LandItr MazeNode::land() const { return p_land; }

const std::vector < int > &MazeNode::visits() const { return p_visits; }

void MazeNode::setLand(LandItr land) { p_land = land; }

void MazeNode::addVisit(int visit) { p_visits.push_back(visit); }

void MazeNode::resetVisits() { p_visits.clear(); }
