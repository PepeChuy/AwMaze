#ifndef SEARCHTREE_H
#define SEARCHTREE_H

#include "searchtreenode.h"

template < class T >
class SearchTree
{
    public:
        SearchTree();
        ~SearchTree();

        const SearchTreeNode < T > *root() const;

        void setRoot(SearchTreeNode < T > *);
        void clear();

    private:
        SearchTreeNode < T > *m_root;
        void clear(SearchTreeNode<T>*);
};

template < class T >
SearchTree < T > :: SearchTree() : m_root(nullptr) {}

template < class T >
SearchTree < T > :: ~SearchTree() {}

template < class T >
const SearchTreeNode < T > *SearchTree < T > :: root() const { return m_root; }

template < class T >
void SearchTree < T > :: setRoot(SearchTreeNode < T > * root) { this->m_root = root; }

template <class T>
void SearchTree<T>::clear()
{
    clear(m_root);
    m_root = nullptr;
}

template <class T>
void SearchTree<T>::clear(SearchTreeNode<T>* root)
{
    for (int i = 0; i < root->children().size(); i++)
    {
        clear(root->children()[i]);
    }
    delete root;
}

#endif // SEARCHTREE_H
