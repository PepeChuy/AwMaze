#include "maze.h"

#include <unordered_map>

Maze::Maze() {}

Maze::CellsGrid &Maze::nodes() { return p_cells; }

const std::vector < Land > &Maze::lands() const { return p_lands; }

void Maze::setLands(std::vector < Land > lands) { p_lands = lands; }

void Maze::initGrid(const std::vector < std::vector < int > > &landMap)
{
    std::unordered_map < int, MazeNode::LandItr > landId2Itr;
    for (auto it = p_lands.cbegin(); it != p_lands.cend(); ++it)
    {
        landId2Itr[it->id()] = it;
    }

    p_cells.clear();
    for (const auto &row : landMap)
    {
        std::vector < MazeNode > newMapRow;
        for (auto landId : row)
        {
            newMapRow.push_back(MazeNode(landId2Itr[landId]));
        }
        p_cells.push_back(newMapRow);
    }
}

MazeNode* Maze::getInitNode()
{
    return this->initNode;
}

void Maze::setInitNode(MazeNode* initNode)
{
    this->initNode = initNode;
}

MazeNode* Maze::getFinalNode()
{
    return this->finalNode;
}

void Maze::setFinalNode(MazeNode* finalNode)
{
    this->finalNode = finalNode;
}

int Maze::getCurrentX() const
{
    return currentX;
}

void Maze::setCurrentX(int value)
{
    currentX = value;
}

int Maze::getCurrentY() const
{
    return currentY;
}

void Maze::setCurrentY(int value)
{
    currentY = value;
}
