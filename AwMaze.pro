#-------------------------------------------------
#
# Project created by QtCreator 2018-09-10T21:47:53
#
#-------------------------------------------------

QT       += core gui widgets svg multimedia
qtHaveModule(opengl): QT += opengl

TARGET = AwMaze
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    mazectrl.cpp \
    loadmazedialog.cpp \
    addbeingdialog.cpp \
    being.cpp \
    beingsctrl.cpp \
    land.cpp \
    mazenode.cpp \
    maze.cpp \
    mazescene.cpp \
    mazenodeinfo.cpp \
    mazenodedata.cpp \
    svgview.cpp \
    exportdialog.cpp \
    svgviewerwindow.cpp \
    autoplayconfig.cpp

HEADERS += \
        mainwindow.h \
    mazectrl.h \
    loadmazedialog.h \
    addbeingdialog.h \
    being.h \
    beingsctrl.h \
    land.h \
    mazenode.h \
    maze.h \
    mazescene.h \
    mazenodeinfo.h \
    searchtreenode.h \
    searchtree.h \
    mazenodedata.h \
    svgview.h \
    exportdialog.h \
    svgviewerwindow.h \
    autoplayconfig.h

FORMS += \
        mainwindow.ui \
    loadmazedialog.ui \
    addbeingdialog.ui \
    mazenodeinfo.ui \
    autoplayconfig.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

DISTFILES +=
