#include "mazescene.h"

MazeScene::MazeScene(QObject *parent) : QGraphicsScene (parent) {}

void MazeScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (mouseEvent->button() == Qt::LeftButton)
    {
        auto *item = itemAt(mouseEvent->scenePos(), QTransform());
        if (item != nullptr) emit itemClicked(item);
    }
}
