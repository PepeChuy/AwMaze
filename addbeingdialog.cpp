#include "addbeingdialog.h"
#include "ui_addbeingdialog.h"

#include <QRadioButton>

AddBeingDialog::AddBeingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddBeingDialog)
{
    ui->setupUi(this);
    ui->label_err->setVisible(false);

    bg = new QButtonGroup(this);
    bg->setExclusive(true);
    QRadioButton *radioButton = new QRadioButton();
    radioButton->setIconSize(QSize(32, 32));
    radioButton->setIcon(QIcon(QPixmap(":/resources/icon.png")));
    radioButton->setChecked(true);
    bg->addButton(radioButton);
    bg->setId(radioButton, 0);
    ui->groupBox->layout()->addWidget(radioButton);

    radioButton = new QRadioButton();
    radioButton->setIconSize(QSize(32, 32));
    radioButton->setIcon(QIcon(QPixmap(":/resources/icon2.png")));
    radioButton->setChecked(true);
    bg->addButton(radioButton);
    bg->setId(radioButton, 1);
    ui->groupBox->layout()->addWidget(radioButton);

    radioButton = new QRadioButton();
    radioButton->setIconSize(QSize(32, 32));
    radioButton->setIcon(QIcon(QPixmap(":/resources/test.png")));
    radioButton->setChecked(true);
    bg->addButton(radioButton);
    bg->setId(radioButton, 2);
    ui->groupBox->layout()->addWidget(radioButton);

    radioButton = new QRadioButton();
    radioButton->setIconSize(QSize(32, 32));
    radioButton->setIcon(QIcon(QPixmap(":/resources/mario.png")));
    radioButton->setChecked(true);
    bg->addButton(radioButton);
    bg->setId(radioButton, 3);
    ui->groupBox->layout()->addWidget(radioButton);
}

AddBeingDialog::~AddBeingDialog()
{
    delete ui;
}

void AddBeingDialog::execMe()
{
    emit requestLands();
    emit requestBeings();
    this->exec();
}

void AddBeingDialog::showErr(QString errDesc)
{
    ui->label_err->setVisible(true);
    ui->label_err->setText(errDesc);
}

void AddBeingDialog::retrieveLands(std::vector<Land> lands)
{
    this->lands = lands;
    int rowsToDelete = ui->tableWidget_lands->rowCount();
    for (auto i = 0; i < rowsToDelete; ++i)
    {
        ui->tableWidget_lands->removeRow(0);
    }
    auto i = 0;
    for (auto l : lands)
    {
        auto name = new QTableWidgetItem(l.name());
        auto pattern = new QTableWidgetItem();
        auto cost = new QLineEdit();
        cost->setMaxLength(MAX_INPUT_LEN);
        pattern->setFlags(pattern->flags() & ~Qt::ItemIsEnabled);
        name->setFlags(pattern->flags() & ~Qt::ItemIsEnabled);
        QColor color = QColor();
        color.setNamedColor(l.pattern());
        pattern->setBackgroundColor(color);
        ui->tableWidget_lands->insertRow(i);
        ui->tableWidget_lands->setItem(i, 0, pattern);
        ui->tableWidget_lands->setItem(i, 1, name);
        ui->tableWidget_lands->setCellWidget(i++, 2, cost);
    }
}

void AddBeingDialog::on_button_addBeing_clicked()
{
    QString name = ui->lineEdit_beingName->text();
    QString iconPath;
    switch (bg->checkedId()) {
        case 0:
            iconPath = ":/resources/icon.png";
            break;
        case 1:
            iconPath = ":/resources/icon2.png";
            break;
        case 2:
            iconPath = ":/resources/test.png";
            break;
        case 3:
            iconPath = ":/resources/mario.png";
            break;
    }
    if (name.isEmpty())
    {
        showErr("Name of the being cannot be empty");
        return;
    }
    for (auto being : this->beings)
    {
        if (being.getName() == name)
        {
            showErr("Name is already taken");
            return;
        }
        if (being.getIconPath() == iconPath)
        {
            showErr("Icon is already taken");
            return;
        }
    }
    std::map<int, double> map = std::map<int, double>();
    for (auto i = 0; i < ui->tableWidget_lands->rowCount(); ++i)
    {
        QString currStr = static_cast < QLineEdit * > (ui->tableWidget_lands->cellWidget(i, 2))->text();
        bool isNumber;
        double cost = currStr.toDouble(&isNumber);
        if (!isNumber)
        {
            if (currStr != "N/A" && currStr != "n/a" && currStr != "na" && currStr !=  "NA")
            {
                showErr(QString("Unrecognized value at row: %1").arg(i+1));
                return;
            }
            cost = -1;
        }
        else if (isNumber && cost < 0)
        {
            showErr(QString("Negative value at row: %1").arg(i+1));
            return;
        }
        char aux[MAX_INPUT_LEN] = "";
        snprintf(aux, MAX_INPUT_LEN, "%.2f", cost);
        cost = QString::fromUtf8(aux).toDouble();
        map[lands[static_cast<unsigned long long>(i)].id()] = cost;
    }
    ui->label_err->setVisible(false);
    auto* being = new Being(name, map, iconPath);
    emit addBeing(*being);
    emit requestBeings();
}

void AddBeingDialog::retrieveBeings(std::vector<Being>& beings)
{
    this->beings = beings;
}
