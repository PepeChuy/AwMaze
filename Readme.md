# **AwMaze**

## Dependencies

* Qt 5.11
* Graphviz (`dot`)

## Supported platforms

* GNU / Linux

    Out of the box!

* macOS

    Untested. Should work out of the box.

* Windows

    Supported. Just make sure that you have access to `dot` from your *PATH*

## Installation

Tested in GNU / Linux. Installation in other platforms should follow the same
principle but could vary.

```bash
git clone git@gitlab.com:PepeChuy/AwMaze.git
qmake
make
./AwMaze
```

## How to play?

Just start the application to start playing!

### Actions overview

When **AwMaze** starts you will be welcomed by the following view:

![Welcome view][Welcome]

From top to bottom, the buttons control the following actions:

1. Load maze ( *Ctrl + L* )
---
2. Play ( *Ctrl + P* )
3. Auto play ( *Ctrl + Shift + P* )
4. Clear game ( *Ctrl + C* )
---
5. Select initial ( *Ctrl + I* )
6. Select final ( *Ctrl + F* )
---
7. Show search tree ( *Ctrl + T* )

### Controlling the music

You can control the music with the following shortcuts:

* *Ctrl + Right Key* to move to the next song
* *Ctrl + Left Key* to move to the previous song
* *Ctrl + Up Key* to increase the volume
* *Ctrl + Down Key* to lower the volume

### Loading a maze

**AwMaze** supports *CSV files*. A file with the following contents would define
a maze with *4 rows*, *7 columns* and *4 lands*.

```
1,3,2,3,1,4,3
2,4,2,4,3,1,2
2,1,3,2,3,4,1
3,4,2,4,3,2,3
```

Lands are identified using non negative integers. The *CSV grid* must be
rectangular and its size can be at most *15x15*.

After loading a maze, you need to name the lands and assign them a color:

![Loading maze view][MazeLoad]

### Setting up beings

Once you load a maze, it will be rendered:

![Maze rendered view][MazeRendered]

Click *Add being* and fill in the information of the being you want to create:

![Creating being view][BeingSetup]

Use the value *NA*, *na*, *N/A* or *n/a* to specify that the being can't go
to a certain land.

### Setting up the game

Once you have a being selected, use the *Select initial* and *Select final*
actions to configure where you want to start from and where you want to go:

![Game setup view][GameSetup]

### Playing

#### Traditional play

Just execute the *Play* action and start playing!

Use the arrow keys to move the being around:

![Traditional play view][TraditionalPlay]

#### Auto play

If you want the computer to play for you just execute the *Auto play* action
and configure how you want it to play:

![Auto play setup view][AutoPlaySetup]

### Resetting the game

You can use the *Clear game* action to start over. The game is automatically
cleared when you change the being and when you load a new maze.

### The search tree

Execute the *Show search tree* action to see a graphical representation of the
search tree whether you've played traditionally or automatically:

![Search tree view][SearchTree]

[Welcome]: docs/img/Welcome.png
[MazeLoad]: docs/img/MazeLoad.png
[MazeRendered]: docs/img/MazeRendered.png
[BeingSetup]: docs/img/BeingSetup.png
[GameSetup]: docs/img/GameSetup.png
[TraditionalPlay]: docs/img/TraditionalPlay.png
[AutoPlaySetup]: docs/img/AutoPlaySetup.png
[SearchTree]: docs/img/SearchTree.png
