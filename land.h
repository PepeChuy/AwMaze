#ifndef LAND_H
#define LAND_H

#include <QString>

class Land
{
    public:
        enum PatternType
        {
            Color,
            Image
        };
        Land();
        Land(int id, QString name, PatternType type, QString pattern);

        int id() const;
        const QString &name() const;
        PatternType type() const;
        const QString &pattern() const;

        void setId(int id);
        void setName(QString name);
        void setType(PatternType type);
        void setPattern(QString pattern);

    private:
        int p_id;
        QString p_name;
        PatternType p_type;
        QString p_pattern;
};

#endif // LAND_H
