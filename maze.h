#ifndef MAZE_H
#define MAZE_H

#include "mazenode.h"
#include "land.h"

#include <vector>

class Maze
{
    public:
        using CellsGrid = std::vector < std::vector < MazeNode > >;
        Maze();

        CellsGrid &nodes();
        const std::vector < Land > &lands() const;

        void setLands(std::vector < Land > lands);
        void initGrid(const std::vector < std::vector < int > > &landMap);

        MazeNode* getInitNode();
        void setInitNode(MazeNode*);

        MazeNode* getFinalNode();
        void setFinalNode(MazeNode*);

        int getCurrentX() const;
        void setCurrentX(int value);

        int getCurrentY() const;
        void setCurrentY(int value);

private:
        CellsGrid p_cells;
        std::vector < Land > p_lands;
        MazeNode* initNode;
        MazeNode* finalNode;
        int currentX;
        int currentY;
};

#endif // MAZE_H
