#ifndef MAZENODEDATA_H
#define MAZENODEDATA_H

#include "land.h"

#include <vector>

class MazeNodeData
{
    public:
        using LandItr = std::vector < Land > :: const_iterator;
        enum MazeNodeType
        {
            Normal,
            Initial,
            Final
        };
        MazeNodeData(MazeNodeType type, int x, int y, int nVisit, LandItr land,
                     double unitCost, double gn, double hn);

        MazeNodeType type() const;
        bool foundPathMember() const;
        int x() const;
        int y() const;
        int nVisit() const;
        LandItr land() const;
        double cost() const;
        double fn() const;
        double gn() const;
        double hn() const;

        void setType(MazeNodeType type);
        void setFoundPathMember(bool member);
        void setX(int x);
        void setY(int y);
        void setNVisit(int nVisit);
        void setLand(LandItr land);
        void setCost(int unitCost);
        void setGN(double gn);
        void setHN(double hn);

    private:
        MazeNodeType m_type;
        bool m_foundPathMember;
        int m_x;
        int m_y;
        int m_nVisit;
        LandItr m_land;
        double m_unitCost;
        double m_gn;
        double m_hn;
};

#endif // MAZENODEDATA_H
