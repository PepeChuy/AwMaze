#include "mazectrl.h"

#include <QFile>
#include <QTextStream>

#include <QThread>
#include <math.h>

#include <QMessageBox>
#include <QStringBuilder>

MazeCtrl::MazeCtrl() : playing(false) {}

void MazeCtrl::loadMazeFromFile(QString path)
{
    QFile src(path);
    if (!src.open(QFile::ReadOnly | QFile::Text))
    {
        emit errAtLoad(QString("The file %1 could not be loaded!").arg(path));
    }
    else
    {
        auto fileName = path.right(path.size() - path.lastIndexOf('/') - 1);
        QTextStream in(&src);
        auto rowSize = -1, rowNumber = 1;
        p_landMap.clear();
        std::set <int> lands;
        while (!in.atEnd())
        {
            auto rowCells = in.readLine().split(",", QString::SkipEmptyParts);
            if (rowCells.size() > MAX_COLS)
            {
                emit errAtLoad(QString("Found %1 cells at row %2! (Max: %3)")
                               .arg(rowCells.size()).arg(rowNumber).arg(MAX_COLS));
                return;
            }
            else if (rowSize == -1)
            {
                rowSize = rowCells.size();
            }
            else if (rowCells.size() != rowSize)
            {
                emit errAtLoad(QString("Found %1 cells at row %2 instead of %3 (found previously)!")
                               .arg(rowCells.size()).arg(rowNumber).arg(rowSize));
                return;
            }

            auto parsedRow = std::vector < int > ();
            bool parseOk;
            for (auto c : rowCells)
            {
                auto parsedCell = c.trimmed().toInt(&parseOk);
                if (!parseOk || parsedCell < 0)
                {
                    emit errAtLoad(QString("Bad cell \"%1\" at row %2!")
                                   .arg(c.trimmed()).arg(rowNumber));
                    return;
                }
                parsedRow.push_back(parsedCell);
                lands.insert(parsedCell);
            }
            p_landMap.push_back(parsedRow);

            ++rowNumber;
        }

        if (p_landMap.size() > MAX_ROWS)
        {
            emit errAtLoad(QString("Found %1 rows! (Max: %2)").arg(MAX_ROWS));
        }
        else
        {
            emit succAtLoad(fileName, lands);
        }
    }
}

void MazeCtrl::landMap2Maze(std::vector < Land > lands)
{
    p_maze.setLands(lands);
    p_maze.initGrid(p_landMap);
    setupBeing("", std::map < int, double > ());
    emit mazeIsReady(p_maze);
}

void MazeCtrl::sendLandsInfo()
{
    emit landsInfo(p_maze.lands());
}

void MazeCtrl::setupBeing(const QString &name, const std::map < int, double > &costs)
{
    p_being.setName(name);
    p_being.setCosts(costs);
    emit beingIsReady();
}

void MazeCtrl::setInit(const int& x, const int& y)
{
    if (y > static_cast<int>(p_maze.nodes().size()) || x > static_cast<int>(p_maze.nodes()[0].size()))
    {
        emit setInitRes(false, x, y);
        return;
    }
    MazeNode* node = &p_maze.nodes()[static_cast<unsigned long long>(y)][static_cast<unsigned long long>(x)];
    if (p_being.getCosts().at(node->land()->id()) < 0)
    {
        emit setInitRes(false, x, y);
        return;
    }
    p_maze.setInitNode(node);
    p_maze.setCurrentX(x);
    p_maze.setCurrentY(y);
    emit setInitRes(true, x, y);
}

void MazeCtrl::setFinal(const int& x, const int& y)
{
    if (y > static_cast<int>(p_maze.nodes().size()) || x > static_cast<int>(p_maze.nodes()[0].size()))
    {
        emit setFinalRes(false, x, y);
        return;
    }
    MazeNode* node = &p_maze.nodes()[static_cast<unsigned long long>(y)][static_cast<unsigned long long>(x)];
    p_maze.setFinalNode(node);
    emit setFinalRes(true, x, y);
}

void MazeCtrl::start()
{
    actualGameMode = GameMode::Normal;
    playing = true;
    maskSetup();
    moveTo(p_maze.getInitNode());
    auto initialCoordinates = getCoordinates(p_maze.getInitNode());
    auto initialNode = new SearchTreeNode<MazeNodeData>(
        nullptr,
        MazeNodeData(
            MazeNodeData::Initial,
            initialCoordinates.first,
            initialCoordinates.second,
            vis,
            p_maze.getInitNode()->land(),
            p_being.getCosts().at(p_maze.getInitNode()->land()->id()),
            -1,
            -1
        )
    );
    searchTree.setRoot(initialNode);
    currentNode = initialNode;
    possiblePaths = getPossiblePaths(p_maze.getInitNode());
    for (int i = 0; static_cast<size_t>(i) < possiblePaths.size(); i++)
    {
        if (possiblePaths[static_cast<size_t>(i)] != nullptr)
        {
            initialNode->children().push_back(possiblePaths[static_cast<size_t>(i)]);
        }
    }
}

void MazeCtrl::searchTree2DotLang()
{
    if (searchTree.root() == nullptr) return;
    int id = 0;
    QString generalShape("ellipse");
    QString out("digraph SearchTree {\n\tforcelabels = true;\n\t");
    out += "graph [fontname = \"Helvetica-Oblique\", fontsize = 20, label = \"\n\n";
    switch (actualGameMode)
    {
        case AStar:
            out += "A*\nf(n) = g(n) + h(n)";
            break;
        case Greedy:
            out += "Greedy\nf(n) = h(n)";
            break;
        case UniformCost:
            out += "Uniform Cost\nf(n) = g(n)";
            break;
        default:
            out += "Walking";
            generalShape = "circle";
            break;
    }
    out += "\"];\n\t";
    out += "node [shape = " + generalShape + ", style = filled];\n\t";
    out += "edge [fontcolor = blue];\n\t";
    out += "{rank = same; \"\"; 1;}\n\t\"\" [style = invisible];\n\t";
    out += "\"\" -> 1;\n";
    out += tree2DotLang(searchTree.root(), id, 0);
    out += "}\n";
    emit searchTreeConverted(out);
}

void MazeCtrl::maskSetup()
{
    auto unmasked = neighborsCoords(p_maze.getCurrentX(), p_maze.getCurrentY());
    unmasked.push_back(std::make_pair(p_maze.getCurrentX(), p_maze.getCurrentY()));
    unmasked.push_back(getCoordinates(p_maze.getFinalNode()));
    emit maskAllNodes(unmasked);
}

void MazeCtrl::moveUp()
{
    if (playing and p_maze.getCurrentY() > 0)
    {
        MazeNode* node = &p_maze.nodes()[static_cast<unsigned long long>(p_maze.getCurrentY()-1)][static_cast<unsigned long long>(p_maze.getCurrentX())];
        if (p_being.getCosts().at(node->land()->id()) >= 0)
        {
            p_maze.setCurrentY(p_maze.getCurrentY()-1);
            emit unmaskNodes(neighborsCoords(p_maze.getCurrentX(), p_maze.getCurrentY()));
            node->addVisit(++vis);
            emit successfulMovement(p_maze.getCurrentX(), p_maze.getCurrentY(), node->visits());
            currentNode = possiblePaths[static_cast<unsigned long long> (std::distance(neighborsOrder.begin(), std::find(neighborsOrder.begin(), neighborsOrder.end(), static_cast<int>(UP))))];
            currentNode->data().setNVisit(vis);
            if (node == p_maze.getFinalNode())
            {
                currentNode->data().setType(MazeNodeData::MazeNodeType::Final);
                QMessageBox::information(nullptr, "You won", "You won!");
                markFoundPath(currentNode);
                return;
            }
            possiblePaths = getPossiblePaths(node);
            for (int i = 0; static_cast<size_t>(i) < possiblePaths.size(); i++)
            {
                if (possiblePaths[static_cast<size_t>(i)] != nullptr)
                {
                    currentNode->children().push_back(possiblePaths[static_cast<size_t>(i)]);
                }
            }
        }
    }
}

void MazeCtrl::moveRight()
{
    if (playing and p_maze.getCurrentX() < static_cast<int>(p_maze.nodes()[0].size())-1)
    {
        MazeNode* node = &p_maze.nodes()[static_cast<unsigned long long>(p_maze.getCurrentY())][static_cast<unsigned long long>(p_maze.getCurrentX()+1)];
        if (p_being.getCosts().at(node->land()->id()) >= 0)
        {
            p_maze.setCurrentX(p_maze.getCurrentX()+1);
            emit unmaskNodes(neighborsCoords(p_maze.getCurrentX(), p_maze.getCurrentY()));
            node->addVisit(++vis);
            emit successfulMovement(p_maze.getCurrentX(), p_maze.getCurrentY(), node->visits());
            currentNode = possiblePaths[static_cast<unsigned long long> (std::distance(neighborsOrder.begin(), std::find(neighborsOrder.begin(), neighborsOrder.end(), static_cast<int>(RIGHT))))];
            currentNode->data().setNVisit(vis);
            if (node == p_maze.getFinalNode())
            {
                currentNode->data().setType(MazeNodeData::MazeNodeType::Final);
                QMessageBox::information(nullptr, "You won", "You won!");
                markFoundPath(currentNode);
                return;
            }
            possiblePaths = getPossiblePaths(node);
            for (int i = 0; static_cast<size_t>(i) < possiblePaths.size(); i++)
            {
                if (possiblePaths[static_cast<size_t>(i)] != nullptr)
                {
                    currentNode->children().push_back(possiblePaths[static_cast<size_t>(i)]);
                }
            }
        }
    }
}

void MazeCtrl::moveDown()
{
    if (playing and p_maze.getCurrentY() < static_cast<int>(p_maze.nodes().size())-1)
    {
        MazeNode* node = &p_maze.nodes()[static_cast<unsigned long long>(p_maze.getCurrentY()+1)][static_cast<unsigned long long>(p_maze.getCurrentX())];
        if (p_being.getCosts().at(node->land()->id()) >= 0)
        {
            p_maze.setCurrentY(p_maze.getCurrentY()+1);
            emit unmaskNodes(neighborsCoords(p_maze.getCurrentX(), p_maze.getCurrentY()));
            node->addVisit(++vis);
            emit successfulMovement(p_maze.getCurrentX(), p_maze.getCurrentY(), node->visits());
            currentNode = possiblePaths[static_cast<unsigned long long> (std::distance(neighborsOrder.begin(), std::find(neighborsOrder.begin(), neighborsOrder.end(), static_cast<int>(DOWN))))];
            currentNode->data().setNVisit(vis);
            if (node == p_maze.getFinalNode())
            {
                currentNode->data().setType(MazeNodeData::MazeNodeType::Final);
                QMessageBox::information(nullptr, "You won", "You won!");
                markFoundPath(currentNode);
                return;
            }
            possiblePaths = getPossiblePaths(node);
            for (int i = 0; static_cast<size_t>(i) < possiblePaths.size(); i++)
            {
                if (possiblePaths[static_cast<size_t>(i)] != nullptr)
                {
                    currentNode->children().push_back(possiblePaths[static_cast<size_t>(i)]);
                }
            }
        }
    }
}

void MazeCtrl::moveLeft()
{
    if (playing and p_maze.getCurrentX() > 0)
    {
        MazeNode* node = &p_maze.nodes()[static_cast<unsigned long long>(p_maze.getCurrentY())][static_cast<unsigned long long>(p_maze.getCurrentX()-1)];
        if (p_being.getCosts().at(node->land()->id()) >= 0)
        {
            p_maze.setCurrentX(p_maze.getCurrentX()-1);
            emit unmaskNodes(neighborsCoords(p_maze.getCurrentX(), p_maze.getCurrentY()));
            node->addVisit(++vis);
            emit successfulMovement(p_maze.getCurrentX(), p_maze.getCurrentY(), node->visits());
            currentNode = possiblePaths[static_cast<unsigned long long> (std::distance(neighborsOrder.begin(), std::find(neighborsOrder.begin(), neighborsOrder.end(), static_cast<int>(LEFT))))];
            currentNode->data().setNVisit(vis);
            if (node == p_maze.getFinalNode())
            {
                currentNode->data().setType(MazeNodeData::MazeNodeType::Final);
                QMessageBox::information(nullptr, "You won", "You won!");
                markFoundPath(currentNode);
                return;
            }
            possiblePaths = getPossiblePaths(node);
            for (int i = 0; static_cast<size_t>(i) < possiblePaths.size(); i++)
            {
                if (possiblePaths[static_cast<size_t>(i)] != nullptr)
                {
                    currentNode->children().push_back(possiblePaths[static_cast<size_t>(i)]);
                }
            }
        }
    }
}

void MazeCtrl::moveTo(MazeNode* destinyNode)
{
    auto coordinates = getCoordinates(destinyNode);
    p_maze.setCurrentX(coordinates.first);
    p_maze.setCurrentY(coordinates.second);
    emit unmaskNodes(neighborsCoords(p_maze.getCurrentX(), p_maze.getCurrentY()));
    destinyNode->addVisit(++vis);
    emit successfulMovement(p_maze.getCurrentX(), p_maze.getCurrentY(), destinyNode->visits());
    if (destinyNode == p_maze.getFinalNode())
    {
        QMessageBox::information(nullptr, "You won", "You won!");
    }
}

void MazeCtrl::sendNodeInfo(int x, int y)
{
    if (p_maze.nodes().empty()) return;
    MazeNode* node = &p_maze.nodes()[static_cast<unsigned long long>(y)][static_cast<unsigned long long>(x)];
    emit sentNodeInfo(*node, x, y, node == p_maze.getInitNode(), node == p_maze.getFinalNode());
}

void MazeCtrl::solveByUniformCost()
{
    auto queue = std::vector<std::pair<MazeNode*, double>>();
    auto treeQueue = std::vector<SearchTreeNode<MazeNodeData>*>();
    auto closed = std::set<MazeNode*>();
    auto cameFrom = std::map<MazeNode*, MazeNode*>();

    actualGameMode = GameMode::UniformCost;

    queue.push_back(std::pair<MazeNode*, double>(p_maze.getInitNode(), 0));
    auto initialCoordinates = getCoordinates(p_maze.getInitNode());
    auto initialNode = new SearchTreeNode<MazeNodeData>(nullptr, MazeNodeData(MazeNodeData::MazeNodeType::Initial, initialCoordinates.first, initialCoordinates.second, -1, p_maze.getInitNode()->land(), p_being.getCosts().at(p_maze.getInitNode()->land()->id()), 0, -1));
    treeQueue.push_back(initialNode);
    searchTree.setRoot(initialNode);
    cameFrom[p_maze.getInitNode()] = p_maze.getInitNode();

    maskSetup();

    while (!queue.empty())
    {
        auto currentNode = queue.front().first;
        auto accumulatedCost = queue.front().second;
        auto currentTreeNode = treeQueue.front();
        auto neighbors = std::vector<MazeNode*>();

        if (closed.find(currentNode) != closed.end() && !repeatingNodes)
        {
            treeQueue.erase(treeQueue.begin());
            queue.erase(queue.begin());
            continue;
        }

        moveTo(currentNode);
        currentTreeNode->data().setNVisit(vis);

        emit processEvents();
        QThread::msleep(MOV_WAIT);

        if (currentNode == p_maze.getFinalNode())
        {
            currentTreeNode->data().setType(MazeNodeData::MazeNodeType::Final);
            treeQueue.clear();
            queue.clear();
            markFoundPath(currentTreeNode);
            emit finishedAutoPlay();
            break;
        }

        queue.erase(queue.begin());
        treeQueue.erase(treeQueue.begin());

        closed.insert(currentNode);

        neighbors = getNeighbors(currentNode);

        for (auto neighbor : neighbors)
        {
            if ((closed.find(neighbor) != closed.end() && !repeatingNodes) || p_being.getCosts().at(neighbor->land()->id()) < 0)
            {
                continue;
            }
            auto cost = accumulatedCost + p_being.getCosts().at(neighbor->land()->id());
            auto newNodeCoordinates = getCoordinates(neighbor);
            auto newTreeNode = new SearchTreeNode<MazeNodeData>(currentTreeNode, MazeNodeData(MazeNodeData::MazeNodeType::Normal, newNodeCoordinates.first, newNodeCoordinates.second, -1, neighbor->land(), p_being.getCosts().at(neighbor->land()->id()), cost, -1));
            currentTreeNode->children().push_back(newTreeNode);
            bool changeOrigin = true;
            bool inserted = false;
            for (unsigned long long i = 0; i < queue.size(); i++)
            {
                if (queue[i].first == neighbor)
                {
                    //TODO: Se puede optimizar si inserted = true y break
                    changeOrigin = false;
                }
                if (cost < queue[i].second)
                {
                    //TODO: Se puede optimizar si elimino entradas posteriores
                    queue.insert(queue.begin()+static_cast<int>(i), std::pair<MazeNode*, double>(neighbor, cost));
                    treeQueue.insert(treeQueue.begin()+static_cast<int>(i), newTreeNode);
                    inserted = true;
                    break;
                }
            }
            if (!inserted)
            {
                queue.push_back(std::pair<MazeNode*, double>(neighbor, cost));
                treeQueue.push_back(newTreeNode);
            }
            if (changeOrigin)
            {
                cameFrom[neighbor] = currentNode;
            }
        }
    }
}

void MazeCtrl::solveByGreedy()
{
    auto queue = std::vector<std::pair<MazeNode*, double>>();
    auto treeQueue = std::vector<SearchTreeNode<MazeNodeData>*>();
    auto closed = std::set<MazeNode*>();
    auto cameFrom = std::map<MazeNode*, MazeNode*>();

    actualGameMode = GameMode::Greedy;

    queue.push_back(std::pair<MazeNode*, double>(p_maze.getInitNode(), getHeuristicDistance(p_maze.getInitNode(), p_maze.getFinalNode())));
    auto initialCoordinates = getCoordinates(p_maze.getInitNode());
    auto initialNode = new SearchTreeNode<MazeNodeData>(nullptr, MazeNodeData(MazeNodeData::MazeNodeType::Initial, initialCoordinates.first, initialCoordinates.second, -1, p_maze.getInitNode()->land(), p_being.getCosts().at(p_maze.getInitNode()->land()->id()), -1, getHeuristicDistance(p_maze.getInitNode(), p_maze.getFinalNode())));
    treeQueue.push_back(initialNode);
    searchTree.setRoot(initialNode);
    cameFrom[p_maze.getInitNode()] = p_maze.getInitNode();

    maskSetup();

    while (!queue.empty())
    {
        auto currentNode = queue.front().first;
        auto currentTreeNode = treeQueue.front();
        auto neighbors = std::vector<MazeNode*>();

        if (closed.find(currentNode) != closed.end() && !repeatingNodes)
        {
            treeQueue.erase(treeQueue.begin());
            queue.erase(queue.begin());
            continue;
        }

        moveTo(currentNode);
        currentTreeNode->data().setNVisit(vis);

        emit processEvents();
        QThread::msleep(MOV_WAIT);

        if (currentNode == p_maze.getFinalNode())
        {
            currentTreeNode->data().setType(MazeNodeData::MazeNodeType::Final);
            treeQueue.clear();
            queue.clear();
            markFoundPath(currentTreeNode);
            emit finishedAutoPlay();
            break;
        }

        queue.erase(queue.begin());
        treeQueue.erase(treeQueue.begin());

        closed.insert(currentNode);

        neighbors = getNeighbors(currentNode);


        for (auto neighbor : neighbors)
        {
            if ((closed.find(neighbor) != closed.end() && !repeatingNodes) || p_being.getCosts().at(neighbor->land()->id()) < 0)
            {
                continue;
            }
            double heuristicCost = getHeuristicDistance(neighbor, p_maze.getFinalNode());
            auto newNodeCoordinates = getCoordinates(neighbor);
            auto newTreeNode = new SearchTreeNode<MazeNodeData>(currentTreeNode, MazeNodeData(MazeNodeData::MazeNodeType::Normal, newNodeCoordinates.first, newNodeCoordinates.second, -1, neighbor->land(), p_being.getCosts().at(neighbor->land()->id()), -1, heuristicCost));
            currentTreeNode->children().push_back(newTreeNode);
            bool shouldInsert = true;
            for (unsigned long long i = 0; i < queue.size(); i++)
            {
                if (queue[i].first == neighbor && !repeatingNodes)
                {
                    shouldInsert = false;
                    break;
                }
                if (heuristicCost < queue[i].second)
                {
                    queue.insert(queue.begin()+static_cast<int>(i), std::pair<MazeNode*, double>(neighbor, heuristicCost));
                    treeQueue.insert(treeQueue.begin()+static_cast<int>(i), newTreeNode);
                    shouldInsert = false;
                    break;
                }
            }
            if (shouldInsert)
            {
                queue.push_back(std::pair<MazeNode*, double>(neighbor, heuristicCost));
                treeQueue.push_back(newTreeNode);
                cameFrom[neighbor] = currentNode;
            }
        }
    }
}

void MazeCtrl::solveByAStar()
{
    auto queue = std::vector<std::pair<MazeNode*, double>>();
    auto treeQueue = std::vector<SearchTreeNode<MazeNodeData>*>();
    auto closed = std::set<MazeNode*>();

    actualGameMode = GameMode::AStar;

    queue.push_back(std::pair<MazeNode*, double>(p_maze.getInitNode(), 0));
    auto initialCoordinates = getCoordinates(p_maze.getInitNode());
    auto initialNode = new SearchTreeNode<MazeNodeData>(nullptr, MazeNodeData(MazeNodeData::MazeNodeType::Initial, initialCoordinates.first, initialCoordinates.second, -1, p_maze.getInitNode()->land(), p_being.getCosts().at(p_maze.getInitNode()->land()->id()), 0, getHeuristicDistance(p_maze.getInitNode(), p_maze.getFinalNode())));
    treeQueue.push_back(initialNode);
    searchTree.setRoot(initialNode);

    maskSetup();

    while (!queue.empty())
    {
        auto currentNode = queue.front().first;
        auto currentTreeNode = treeQueue.front();
        auto accumulatedCost = queue.front().second;
        auto neighbors = std::vector<MazeNode*>();

        if (closed.find(currentNode) != closed.end() && !repeatingNodes)
        {
            treeQueue.erase(treeQueue.begin());
            queue.erase(queue.begin());
            continue;
        }

        moveTo(currentNode);
        currentTreeNode->data().setNVisit(vis);

        emit processEvents();
        QThread::msleep(MOV_WAIT);

        if (currentNode == p_maze.getFinalNode())
        {
            currentTreeNode->data().setType(MazeNodeData::MazeNodeType::Final);
            treeQueue.clear();
            queue.clear();
            markFoundPath(currentTreeNode);
            emit finishedAutoPlay();
            break;
        }

        queue.erase(queue.begin());
        treeQueue.erase(treeQueue.begin());

        closed.insert(currentNode);

        neighbors = getNeighbors(currentNode);

        for (auto neighbor : neighbors)
        {
            if ((closed.find(neighbor) != closed.end() && !repeatingNodes) || p_being.getCosts().at(neighbor->land()->id()) < 0)
            {
                continue;
            }
            auto realCost = accumulatedCost + p_being.getCosts().at(neighbor->land()->id());
            auto totalCost = realCost + getHeuristicDistance(neighbor, p_maze.getFinalNode());
            auto newNodeCoordinates = getCoordinates(neighbor);
            auto newTreeNode = new SearchTreeNode<MazeNodeData>(currentTreeNode, MazeNodeData(MazeNodeData::MazeNodeType::Normal, newNodeCoordinates.first, newNodeCoordinates.second, -1, neighbor->land(), p_being.getCosts().at(neighbor->land()->id()), realCost, getHeuristicDistance(neighbor, p_maze.getFinalNode())));
            currentTreeNode->children().push_back(newTreeNode);
            bool inserted = false;
            for (unsigned long long i = 0; i < queue.size(); i++)
            {
                if (totalCost < queue[i].second + getHeuristicDistance(queue[i].first, p_maze.getFinalNode()))
                {
                    queue.insert(queue.begin()+static_cast<int>(i), std::pair<MazeNode*, double>(neighbor, realCost));
                    treeQueue.insert(treeQueue.begin()+static_cast<int>(i), newTreeNode);
                    inserted = true;
                    break;
                }
            }
            if (!inserted)
            {
                queue.push_back(std::pair<MazeNode*, double>(neighbor, realCost));
                treeQueue.push_back(newTreeNode);
            }
        }
    }
}

std::vector<MazeNode*> MazeCtrl::getNeighbors(MazeNode* node)
{
    auto neighbors = std::vector<MazeNode*>();
    auto coordinates = getCoordinates(node);

    //Obtener e insertar vecinos
    for (auto order : neighborsOrder)
    {
        switch (order)
        {
            case LEFT:
                if (coordinates.first - 1 >= 0)
                {
                    neighbors.push_back(&p_maze.nodes()[static_cast<unsigned long long>(coordinates.second)][static_cast<unsigned long long>(coordinates.first - 1)]);
                }
                break;
            case UP:
                if (coordinates.second - 1 >= 0)
                {
                    neighbors.push_back(&p_maze.nodes()[static_cast<unsigned long long>(coordinates.second - 1)][static_cast<unsigned long long>(coordinates.first)]);
                }
                break;
            case RIGHT:
                if (coordinates.first + 1 < static_cast<int>(p_maze.nodes()[0].size()))
                {
                    neighbors.push_back(&p_maze.nodes()[static_cast<unsigned long long>(coordinates.second)][static_cast<unsigned long long>(coordinates.first + 1)]);
                }
                break;
            case DOWN:
                if (coordinates.second + 1 < static_cast<int>(p_maze.nodes().size()))
                {
                    neighbors.push_back(&p_maze.nodes()[static_cast<unsigned long long>(coordinates.second + 1)][static_cast<unsigned long long>(coordinates.first)]);
                }
                break;
        }
    }

    return neighbors;
}

std::vector<SearchTreeNode<MazeNodeData>*> MazeCtrl::getPossiblePaths(MazeNode* node)
{
    auto possiblePaths = std::vector<SearchTreeNode<MazeNodeData>*>();
    auto coordinates = getCoordinates(node);

    for (auto order : neighborsOrder)
    {
        switch (order)
        {
            case LEFT:
                if (coordinates.first - 1 >= 0)
                {
                    auto mazeNode = &p_maze.nodes()[static_cast<unsigned long long>(coordinates.second)][static_cast<unsigned long long>(coordinates.first - 1)];
                    if (p_being.getCosts().at(mazeNode->land()->id()) >= 0)
                    {
                        possiblePaths.push_back(new SearchTreeNode<MazeNodeData>(currentNode, MazeNodeData(MazeNodeData::MazeNodeType::Normal, coordinates.first - 1, coordinates.second, -1, mazeNode->land(), p_being.getCosts().at(mazeNode->land()->id()), -1, -1)));
                    }
                    else
                    {
                        possiblePaths.push_back(nullptr);
                    }
                }
                else
                {
                    possiblePaths.push_back(nullptr);
                }
                break;
            case UP:
                if (coordinates.second - 1 >= 0)
                {
                    auto mazeNode = &p_maze.nodes()[static_cast<unsigned long long>(coordinates.second - 1)][static_cast<unsigned long long>(coordinates.first)];
                    if (p_being.getCosts().at(mazeNode->land()->id()) >= 0)
                    {
                        possiblePaths.push_back(new SearchTreeNode<MazeNodeData>(currentNode, MazeNodeData(MazeNodeData::MazeNodeType::Normal, coordinates.first, coordinates.second - 1, -1, mazeNode->land(), p_being.getCosts().at(mazeNode->land()->id()), -1, -1)));
                    }
                    else
                    {
                        possiblePaths.push_back(nullptr);
                    }
                }
                else
                {
                    possiblePaths.push_back(nullptr);
                }
                break;
            case RIGHT:
                if (coordinates.first + 1 < static_cast<int>(p_maze.nodes()[0].size()))
                {
                    auto mazeNode = &p_maze.nodes()[static_cast<unsigned long long>(coordinates.second)][static_cast<unsigned long long>(coordinates.first + 1)];
                    if (p_being.getCosts().at(mazeNode->land()->id()) >= 0)
                    {
                        possiblePaths.push_back(new SearchTreeNode<MazeNodeData>(currentNode, MazeNodeData(MazeNodeData::MazeNodeType::Normal, coordinates.first + 1, coordinates.second, -1, mazeNode->land(), p_being.getCosts().at(mazeNode->land()->id()), -1, -1)));
                    }
                    else
                    {
                        possiblePaths.push_back(nullptr);
                    }
                }
                else
                {
                    possiblePaths.push_back(nullptr);
                }
                break;
            case DOWN:
                if (coordinates.second + 1 < static_cast<int>(p_maze.nodes().size()))
                {
                    auto mazeNode = &p_maze.nodes()[static_cast<unsigned long long>(coordinates.second + 1)][static_cast<unsigned long long>(coordinates.first)];
                    if (p_being.getCosts().at(mazeNode->land()->id()) >= 0)
                    {
                        possiblePaths.push_back(new SearchTreeNode<MazeNodeData>(currentNode, MazeNodeData(MazeNodeData::MazeNodeType::Normal, coordinates.first, coordinates.second + 1, -1, mazeNode->land(), p_being.getCosts().at(mazeNode->land()->id()), -1, -1)));
                    }
                    else
                    {
                        possiblePaths.push_back(nullptr);
                    }
                }
                else
                {
                    possiblePaths.push_back(nullptr);
                }
                break;
        }
    }

    return possiblePaths;
}

std::vector < std::pair < int, int > > MazeCtrl::neighborsCoords(int x, int y)
{
    std::vector < std::pair < int, int > > res = {};
    // Left
    if (x > 0)
    {
        res.push_back(std::make_pair(x - 1, y));
    }
    // Up
    if (y > 0)
    {
        res.push_back(std::make_pair(x, y - 1));
    }
    // Right
    if (static_cast < size_t > (x) < (p_maze.nodes()[0].size() - 1))
    {
        res.push_back(std::make_pair(x + 1, y));
    }
    // Down
    if (static_cast < size_t > (y) < (p_maze.nodes().size() - 1))
    {
        res.push_back(std::make_pair(x, y + 1));
    }
    return res;
}

std::pair<int, int> MazeCtrl::getCoordinates(MazeNode* node)
{
    for (unsigned long long i = 0; i < p_maze.nodes().size(); i++)
    {
        for (unsigned long long j = 0; j < p_maze.nodes()[0].size(); j++)
        {
            if (&p_maze.nodes()[i][j] == node)
            {
                return std::pair<int, int>(j, i);
            }
        }
    }
    return std::pair<int, int>(-2, -2);
}

double MazeCtrl::getHeuristicDistance(MazeNode* origin, MazeNode* destiny)
{
    auto originCoordinates = getCoordinates(origin);
    auto destinyCoordinates = getCoordinates(destiny);
    switch (heuristicType)
    {
        case EUCLIDEAN:
            return sqrt(pow(destinyCoordinates.first - originCoordinates.first, 2) + pow(destinyCoordinates.second - originCoordinates.second, 2));
        case MANHATTAN:
            return abs(destinyCoordinates.first - originCoordinates.first) + abs(destinyCoordinates.second - originCoordinates.second);
        default:
            return 0;
    }
}

QString MazeCtrl::tree2DotLang(const SearchTreeNode < MazeNodeData > *node,
                               int &lastID, int parentID)
{
    auto id = ++lastID;
    auto idStr = QString::number(id);
    auto coords = QString("%1%2").arg(QChar('A' + node->data().x())).arg(node->data().y() + 1);
    auto visit = node->data().nVisit();
    auto visitStr = visit != -1 ? QString("xlabel = \"%1\", ").arg(QString::number(visit)) : "";
    auto finalStr = node->data().type() == MazeNodeData::Final ? "shape = doublecircle, " : "";
    QString heuristicInfo = "";
    switch (actualGameMode)
    {
        case AStar:
            heuristicInfo = QString("\n%1 + %2 = %3")
                            .arg(node->data().gn(), 0, 'f', 2)
                            .arg(node->data().hn(), 0, 'f', 2)
                            .arg(node->data().fn(), 0, 'f', 2);
            break;
        case Greedy:
            heuristicInfo = QString("\n%1").arg(node->data().hn(), 0, 'f', 2);
            break;
        case UniformCost:
            heuristicInfo = QString("\n%1").arg(node->data().gn(), 0, 'f', 2);
            break;
        default:
            break;
    }
    auto out = "\t" + idStr +
        "[label=\"" + coords + heuristicInfo + "\", " + visitStr + finalStr +
        "fillcolor=\"" + node->data().land()->pattern() + "\"];\n";

    if (parentID > 0)
    {
        out += "\t" + QString::number(parentID) + " -> " + idStr +
               QString(" [label = \"%1\"").arg(node->data().cost(), 0, 'f', 2) +
               (node->data().foundPathMember() ? ", color = green3" : "") +
               "];\n";
    }

    for (auto ch : node->children())
    {
        out += (tree2DotLang(ch, lastID, id));
    }
    return out;
}

void MazeCtrl::markFoundPath(SearchTreeNode<MazeNodeData>* node)
{
    if (node == nullptr) return;
    node->data().setFoundPathMember(true);
    markFoundPath(node->parent());
}

void MazeCtrl::setNeighborsOrder(std::vector<int> newOrder)
{
    neighborsOrder = newOrder;
}

void MazeCtrl::setHeuristicType(int newHeuristicType)
{
    heuristicType = newHeuristicType;
}

void MazeCtrl::setNodeRepeating(bool repeat)
{
    repeatingNodes = repeat;
}

void MazeCtrl::clearGame()
{
    vis = 0;
    for (unsigned long long i = 0; i < p_maze.nodes().size(); i++)
    {
        for (unsigned long long j = 0; j < p_maze.nodes()[0].size(); j++)
        {
            p_maze.nodes()[i][j].resetVisits();
        }
    }
}
