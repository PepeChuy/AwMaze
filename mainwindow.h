#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "being.h"
#include "maze.h"

#include <unordered_map>
#include <tuple>

#include <QMainWindow>
#include <QStandardPaths>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QShortcut>
#include <QColor>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QButtonGroup>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    signals:
        void showLoadDlg();
        void showBeingDlg();
        void showConfigDlg();
        void showAutoPlayDlg();
        void renderSearchTree(QString svgFileName);
        void selectedBeing(int);
        void trySelectInitial(int x, int y);
        void trySelectFinal(int x, int y);
        void startedPlaying();
        void retrieveSearchTree();
        void showInfo(int x, int y);
        void moveUp();
        void moveRight();
        void moveDown();
        void moveLeft();
        void solveByUniformCost();
        void solveByGreedy();
        void solveByAStar();
        void gameCleared();

    public slots:
        void setBeingsRadios(std::vector<Being>&);
        void onBeingToggled(bool);
        void drawMaze(Maze &mz);
        void showBeingWidget();
        void getSetInitRes(bool succ, int x, int y);
        void getSetFinalRes(bool succ, int x, int y);
        void maskAllNodes(std::vector < std::pair < int, int > > exceptCoords);
        void unmaskNodes(std::vector < std::pair < int, int > > coords);
        void gotSearchTree(QString dotLangRepr);
        void clearGame();
        void moveBeing(int x, int y, std::vector < int > visits);
        void updateBeingIconPath(const QString&);
        void processEvents();
        void setPlayModesEnabled();
        void setClearGameEnabled();

    private slots:
        void on_actionLoadMaze_triggered();
        void on_actionClearGame_triggered();
        void on_addBeingButton_clicked();
        void on_actionSelectInitial_toggled(bool arg1);
        void on_actionSelectFinal_toggled(bool arg1);
        void processItemClick(QGraphicsItem *item);
        void on_actionPlayMaze_triggered();
        void on_actionAutoPlay_triggered();
        void on_actionShowSearchTree_triggered();

    private:
            Ui::MainWindow *ui;
            std::unordered_map < QGraphicsItem *, std::tuple < int, int > > cells;
            std::vector < std::vector < QGraphicsRectItem * > > masks;
            std::vector < std::vector < std::vector < QGraphicsTextItem * > > > visitsTexts;
            QGraphicsPixmapItem *init;
            QGraphicsPixmapItem *final;
            QGraphicsPixmapItem *being;
            QShortcut *up;
            QShortcut *right;
            QShortcut *down;
            QShortcut *left;
            QShortcut *nextSong;
            QShortcut *prevSong;
            QShortcut *volumeUp;
            QShortcut *volumeDown;
            QString beingIconPath;
            size_t rowsN;
            size_t colsN;
            int nodeSz;
            const QColor MASK_COLOR = Qt::black;

            QMediaPlayer* mediaPlayer;
            QMediaPlaylist* mediaPlaylist;
            int currentVolume;

            QButtonGroup* bg;

            void togglePlayAndShowTreeActions(bool enable);
            void toggleSelectStates(bool enable);
            void initGame();
            void renderNodeVisits(int x, int y, const std::vector < int > &visits);
};

#endif // MAINWINDOW_H
