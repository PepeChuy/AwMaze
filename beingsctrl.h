#ifndef BEINGSCTRL_H
#define BEINGSCTRL_H

#include "being.h"
#include "maze.h"

#include <QObject>

class BeingsCtrl : public QObject
{
    Q_OBJECT

    public:
        BeingsCtrl();

    signals:
        void setBeingsRadios(std::vector<Being>&);
        void setCosts(const QString&, const std::map<int, double>&);
        void sendBeings(std::vector<Being>&);
        void newBeingPath(const QString&);

    public slots:
        void addBeing(Being&);
        void selectedBeing(int);
        void requestBeings();
        void deleteBeings(const Maze &mz);

    private:
        std::vector<Being> beings;
};

#endif // BEINGSCTRL_H
