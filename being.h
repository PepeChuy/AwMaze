#ifndef BEING_H
#define BEING_H

#include <QString>
#include <map>


class Being
{
    public:
        Being();
        Being(const QString&, const std::map<int, double>&, const QString&);

        const QString &getName() const;
        void setName(const QString&);

        const std::map<int, double> &getCosts() const;
        void setCosts(const std::map<int, double>&);

        const QString &getIconPath() const;
        void setIconPath(const QString&);

    private:
        QString name;
        std::map<int, double> costs;
        QString iconPath;
};

#endif // BEING_H
