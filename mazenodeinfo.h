#ifndef MAZENODEINFO_H
#define MAZENODEINFO_H

#include "mazenode.h"

#include <QDialog>

namespace Ui {
class MazeNodeInfo;
}

class MazeNodeInfo : public QDialog
{
    Q_OBJECT

    public:
        explicit MazeNodeInfo(QWidget *parent = nullptr);
        ~MazeNodeInfo();

    public slots:
        void execMe(const MazeNode&, int, int, bool, bool);

    private:
        Ui::MazeNodeInfo *ui;
};

#endif // MAZENODEINFO_H
