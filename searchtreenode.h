#ifndef SEARCHTREENODE_H
#define SEARCHTREENODE_H

#include <vector>

template < class T >
class SearchTreeNode
{
    public:
        SearchTreeNode(SearchTreeNode < T > *parent, T data);

        const T &data() const;
        T &data();
        const SearchTreeNode < T > *parent() const;
        SearchTreeNode < T > *parent();
        const std::vector < SearchTreeNode < T > * > &children() const;
        std::vector < SearchTreeNode < T > * > &children();

    private:
        T m_data;
        SearchTreeNode < T > *m_parent;
        std::vector < SearchTreeNode < T > * > m_children;
};

template < class T >
SearchTreeNode < T > :: SearchTreeNode(SearchTreeNode < T > *parent, T data)
    : m_data(data), m_parent(parent) {}

template < class T >
const T &SearchTreeNode < T > :: data() const { return m_data; }

template < class T >
T &SearchTreeNode < T > :: data() { return m_data; }

template < class T >
const SearchTreeNode < T > *SearchTreeNode < T > :: parent() const { return m_parent; }

template < class T >
SearchTreeNode < T > *SearchTreeNode < T > :: parent() { return m_parent; }

template < class T >
const std::vector < SearchTreeNode < T > * > &SearchTreeNode < T > :: children() const
{
    return m_children;
}

template < class T >
std::vector < SearchTreeNode < T > * > &SearchTreeNode < T > :: children() { return m_children; }

#endif // SEARCHTREENODE_H
